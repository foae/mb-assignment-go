package messagebird

import (
	"fmt"
	"math/rand"
	"net/http"
	"net/url"
	"regexp"
	"strings"
)

var (
	MessageBirdURL     = "https://rest.messagebird.com/messages"
	MessageBirdApiKey  = "JezQTtxk3rmEdR9vk9XrT0Gl0"
	MessageBirdTestURL = "http://ptsv2.com/t/8p9sv-1518785110/post" // Test endpoint
)

// Shape of the incoming message
type IncomingMessage struct {
	Recipient  string
	Body       string
	Originator string
}

type Sms struct {
	Originator string   `json:"originator"`
	Recipient  []string `json:"recipients"`
	Body       []string `json:"body"`

	TypeDetails []string `json:"type_details"`
	Type        string   `json:"type"`

	Udh `json:"udh"`
}

type Udh struct {
	Length           string `json:"length"`
	Iei              string `json:"iei"`
	IeiLength        string `json:"iei_length"`
	Csms             string `json:"csms"`
	TotalParts       string `json:"total_parts"`
	ThisPartSequence string `json:"this_part_sequence"`
}

// Make up UDH header
func (mb *Sms) buildUdh() {

	mb.Udh = Udh{
		Length:           fmt.Sprintf("%02v", 5),
		Iei:              fmt.Sprintf("%02v", 0),
		IeiLength:        fmt.Sprintf("%02v", 3),
		Csms:             fmt.Sprintf("%02X", rand.Intn(255)),
		TotalParts:       fmt.Sprintf("%02v", len(mb.Body)),
		ThisPartSequence: fmt.Sprintf("%02v", 1),
	}
}

// Construct a new Sms from an incoming message
func NewFrom(localMsg IncomingMessage) Sms {

	// Assign incoming localMsg to our Sms struct
	mb := Sms{
		Originator: localMsg.Originator,
		Recipient:  strings.Split(localMsg.Recipient, ","),
		Body:       strings.Split(localMsg.Body, ""),
	}

	// Split body of the message into 153 length chunks
	// Assign the chunks to the
	mb.splitBodyAtLength(153)

	// Attach the udh header
	mb.buildUdh()

	return mb
}

// Sms > validate
func (mb *Sms) Validate() error {

	validRecipient := regexp.MustCompile("[0-9]{10,}")
	validOriginator := regexp.MustCompile("^.{1,32}$")
	validBody := regexp.MustCompile("(.*)+")

	validationMap := map[string][]string{
		"Recipient":  validRecipient.FindStringSubmatch(strings.Join(mb.Recipient, "")),
		"Originator": validOriginator.FindStringSubmatch(mb.Originator),
		"Body":       validBody.FindStringSubmatch(strings.Join(mb.Body, "")),
	}

	for key, value := range validationMap {
		if value == nil {
			return fmt.Errorf("incorrect value sent for the %s", key)
		}
	}

	return nil
}

// Split the string chunks
func (mb *Sms) splitBodyAtLength(l int) (parts []string, totalParts int) {

	step := l
	firstIndex := 0
	lastIndex := l
	totalParts = 0

	maxLen := len(mb.Body)
	bodyString := strings.Join(mb.Body, "")

	for {
		totalParts++

		// Best case scenario, message is short
		if maxLen <= step {
			parts = append(parts, bodyString[0:maxLen])
			break
		}

		// Append the "remainder"
		if lastIndex > maxLen {
			parts = append(parts, bodyString[firstIndex:maxLen])
			break
		}

		parts = append(parts, bodyString[firstIndex:lastIndex])
		firstIndex = lastIndex
		lastIndex += step
	}

	mb.Body = parts

	return
}

// Send the HTTP Post request towards MessageBird's API
func (mb *Sms) Send(client *http.Client, addr string) (resp *http.Response, err error) {

	// Relay on the package defaults
	if addr == "" {
		addr = MessageBirdTestURL
	}

	resp, err = client.PostForm(addr, url.Values{
		"recipients": mb.Recipient,
		"originator": {mb.Originator},
		"body":       mb.Body,
	})

	defer resp.Body.Close()
	return
}
