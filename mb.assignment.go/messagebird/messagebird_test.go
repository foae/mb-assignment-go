package messagebird

import (
	"fmt"
	"io/ioutil"
	"net/http"
	"net/http/httptest"
	"reflect"
	"strings"
	"testing"
)

const (
	ANSI_RESET             string = "\u001B[0m"
	ANSI_BLACK             string = "\u001B[30m"
	ANSI_RED               string = "\u001B[31m"
	ANSI_GREEN             string = "\u001B[32m"
	ANSI_YELLOW            string = "\u001B[33m"
	ANSI_BLUE              string = "\u001B[34m"
	ANSI_PURPLE            string = "\u001B[35m"
	ANSI_CYAN              string = "\u001B[36m"
	ANSI_WHITE             string = "\u001B[37m"
	ANSI_BLACK_BACKGROUND  string = "\u001B[40m"
	ANSI_RED_BACKGROUND    string = "\u001B[41m"
	ANSI_GREEN_BACKGROUND  string = "\u001B[42m"
	ANSI_YELLOW_BACKGROUND string = "\u001B[43m"
	ANSI_BLUE_BACKGROUND   string = "\u001B[44m"
	ANSI_PURPLE_BACKGROUND string = "\u001B[45m"
	ANSI_CYAN_BACKGROUND   string = "\u001B[46m"
	ANSI_WHITE_BACKGROUND  string = "\u001B[47m"
)

func Test_AlwaysPass(t *testing.T) {
	fmt.Println("Running Test_AlwaysPass...")
	fmt.Println(ANSI_GREEN_BACKGROUND + " ✓ " + ANSI_RESET)
}

func TestSms_Send(t *testing.T) {

	fmt.Println("Running TestSms_Send...")

	sms := NewFrom(IncomingMessage{
		Recipient:  "0123456789",
		Body:       "Hi, there!",
		Originator: "0123456789",
	})

	// Start the server
	ts := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		body, _ := ioutil.ReadAll(r.Body)
		// Something like... go-querystring is Go library for encoding structs into URL query parameters.
		want := []byte("body=Hi%2C+there%21&originator=0123456789&recipients=0123456789")
		if !reflect.DeepEqual(want, body) {
			t.Errorf("Got %s, wanted %s", body, want)
		}
	}))
	defer ts.Close()

	// Send the request
	c := &http.Client{}
	res, err := func(c *http.Client, url string) (*http.Response, error) {
		fmt.Println("Dispatching request...")
		return sms.Send(c, url)
	}(c, ts.URL)

	if err != nil {
		t.Errorf("Error while Send()ing: %s", err.Error())
		return
	}
	defer res.Body.Close()
}

func Test_splitStringAtLength(t *testing.T) {

	bodyShort := "A while back I needed to count the amount of letters that a piece of text in an email template had (to avoid passing any character limits). Unfortunately"                                                                                                 // 153
	bodyLong := "A while back I needed to count the amount of letters that a piece of text in an email template had (to avoid passing any character limits). Unfortunately, I could not think of a quick way to do so on my macbook and I therefore turned to the Internet." // 250

	smsShort := NewFrom(IncomingMessage{
		Recipient:  "0123456789",
		Body:       bodyShort,
		Originator: "0123456789",
	})

	smsLong := NewFrom(IncomingMessage{
		Recipient:  "0123456789",
		Body:       bodyLong,
		Originator: "0123456789",
	})

	// Check parts
	// Max 1 part for short and 2 parts for long
	if len(smsShort.Body) != 1 {
		t.Errorf("Len of [%d] body exceeds [%d] parts", len(smsShort.Body), 1)
	}

	if len(smsLong.Body) != 2 {
		t.Errorf("Len of [%d] body exceeds [%d] parts", len(smsLong.Body), 2)
	}

	// Check length
	assembledLengthShort := len(strings.Join(smsShort.Body, ""))
	if assembledLengthShort != len(bodyShort) {
		t.Errorf("Len of [%d] body exceeds length of [%d]", assembledLengthShort, len(bodyShort))
	}

	assembledLengthLong := len(strings.Join(smsLong.Body, ""))
	if assembledLengthLong != len(bodyLong) {
		t.Errorf("Len of [%d] body exceeds length of [%d]", assembledLengthLong, len(bodyLong))
	}

	// Check content
	// Assembled back, should render the exact same content as the original
	if strings.Join(smsShort.Body, "") != bodyShort {
		t.Errorf("Assembled body [%s] does not equal expected [%s]", strings.Join(smsShort.Body, ""), bodyShort)
	}

	if strings.Join(smsLong.Body, "") != bodyLong {
		t.Errorf("Assembled body [%s] does not equal expected [%s]", strings.Join(smsLong.Body, ""), bodyLong)
	}

}

func Test_NewFrom(t *testing.T) {

	msg := IncomingMessage{
		Recipient:  "0123456789",
		Body:       "A body to get along with",
		Originator: "0123456789",
	}
	sms := NewFrom(msg)

	if reflect.DeepEqual(strings.Join(sms.Body, ""), msg.Body) == false {
		t.Errorf("msg differes from the built sms. Wanted [%s], got [%s]", msg.Body, sms.Body)
	}
}

func Test_BuildUdh(t *testing.T) {

	msg := IncomingMessage{
		Recipient:  "0123456789",
		Body:       "A body and a udh header to get along with",
		Originator: "0123456789",
	}

	sms := NewFrom(msg)
	sms.buildUdh()

	expected := map[string]string{
		"Length":    fmt.Sprintf("%02v", 5),
		"Iei":       fmt.Sprintf("%02v", 0),
		"IeiLength": fmt.Sprintf("%02v", 3),
	}

	v := reflect.ValueOf(sms.Udh)
	for key := range expected {
		if reflect.DeepEqual(v.FieldByName(key).Interface(), expected[key]) == false {
			t.Errorf("Expected %s, got %s for key %s", expected[key], v.FieldByName(key).Interface(), key)
		}
	}

}

func TestSms_Validate(t *testing.T) {

	msgRecipient := IncomingMessage{
		Recipient:  "321",
		Body:       "A body and a udh header to get along with",
		Originator: "0123456789",
	}
	smsRecipient := NewFrom(msgRecipient)
	if err := smsRecipient.Validate(); err == nil {
		t.Errorf("Expecting nil error, got real error [%v]", err)
	}

	msgBody := IncomingMessage{
		Recipient:  "0123456789",
		Body:       "",
		Originator: "0123456789",
	}
	smsBody := NewFrom(msgBody)
	if err := smsBody.Validate(); err != nil {
		t.Errorf("Expecting error, got real error [%v]", err)
	}

	msgOriginator := IncomingMessage{
		Recipient:  "0123456789",
		Body:       "A body and a udh header to get along with",
		Originator: "123",
	}
	smsOriginator := NewFrom(msgOriginator)
	if err := smsOriginator.Validate(); err != nil {
		t.Errorf("Expecting error, got real error [%v]", err)
	}

}
