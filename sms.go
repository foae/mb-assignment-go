package main

import (
	"encoding/json"
	"fmt"
	"net/http"
	"time"

	"mb.assignment.go/messagebird"
)

/*
DONE:
● Accept POST to /sms only
● Basic validation - no empty or incorrect parameter values are send to MessageBird (input validation)
● Respond with proper JSON message and HTTP status code
● When an incoming message content/body is longer than 160 chars, split it into multiple parts (known as concatenated SMS)
● Move things in their own packages

TODO:
● Write some simple tests. Maybe httptest lib
*/

// Wrapper
type APIHandler struct {
	MessageBirdClient *http.Client
}

// Message returned to the client
type ResponseMessage struct {
	Body string          `json:"status"`
	Sms  messagebird.Sms `json:"sms"`
}

// Handle error output
func (m ResponseMessage) handleError(writer *http.ResponseWriter, body string, statusCode int) {
	(*writer).WriteHeader(statusCode)
	m.Body = body
	output, _ := json.Marshal(m)
	(*writer).Write(output)
}

func main() {
	apiHandler := &APIHandler{MessageBirdClient: &http.Client{}}
	http.HandleFunc("/", apiHandler.incomingRequestHandler)
	http.ListenAndServe(":8000", nil)
}

func (apiHandler *APIHandler) incomingRequestHandler(writer http.ResponseWriter, request *http.Request) {

	fmt.Printf("[%s] --> %s %s\n", time.Now().Format(time.RFC1123), request.Method, request.URL.Path)

	// Setup some defaults for starters
	writer.Header().Set("Content-Type", "application/json")
	responseMessage := ResponseMessage{Body: "OK"}

	// Respond only to /sms
	if request.URL.Path != "/sms" {
		http.NotFound(writer, request)
		return
	}

	// Unpack incoming json
	dec := json.NewDecoder(request.Body)

	// Pre-validation. Write from POST into our Message struct
	incomingMessage := struct {
		// Incoming Message structure
		Recipient  string `json:"recipient"`
		Body       string `json:"message"`
		Originator string `json:"originator"`
	}{}

	if errJson := dec.Decode(&incomingMessage); errJson != nil {
		responseMessage.handleError(&writer, errJson.Error(), http.StatusBadRequest)
		return
	}

	// Adapt POSTed message to the MessageBird's API requirements
	sms := messagebird.NewFrom(struct {
		Recipient  string
		Body       string
		Originator string
	}{
		Recipient:  incomingMessage.Recipient,
		Body:       incomingMessage.Body,
		Originator: incomingMessage.Originator,
	})

	// Validation. Check if the received data is valid
	if err := sms.Validate(); err != nil {
		responseMessage.handleError(&writer, err.Error(), http.StatusBadRequest)
		return
	}

	//POST the built message to the MessageBird's API
	if _, err := sms.Send(apiHandler.MessageBirdClient, ""); err != nil {
		responseMessage.handleError(&writer, err.Error(), http.StatusInternalServerError)
		return
	}

	// Final step: outMessage -> json serialize and output
	// For visibility, we include the "sms" sent to the MB API back to the client
	responseMessage.Sms = sms
	output, _ := json.Marshal(responseMessage)
	writer.Write(output)

}
